//import './module/sw-cms/blocks/text/two-col-banner-element';
//import './module/sw-cms/elements/two-col-banner-element';
import './module/sw-cms/blocks/text-image/three-col-icon-element';
import './module/sw-cms/elements/three-col-icon-element';
import deDE from './module/sw-cms/snippet/de-DE.json';
import enGB from './module/sw-cms/snippet/en-GB.json';

Shopware.Locale.extend('de-DE', deDE);
Shopware.Locale.extend('en-GB', enGB);
