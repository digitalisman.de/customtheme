import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'three-col-icon-element',
    label: 'sw-cms.blocks.text-image.threeColIconElement.label',
    category: 'text-image',
    component: 'sw-cms-block-three-col-icon-element',
    previewComponent: 'sw-cms-preview-three-col-icon-element',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
	    left: 'three-col-icon-element',
	    middle: 'three-col-icon-element',
	    right: 'three-col-icon-element'
    }
});
