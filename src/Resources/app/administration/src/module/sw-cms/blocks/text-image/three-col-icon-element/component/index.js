import template from './sw-cms-block-three-col-icon-element.html.twig';
import './sw-cms-block-three-col-icon-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-three-col-icon-element', {
    template
});
