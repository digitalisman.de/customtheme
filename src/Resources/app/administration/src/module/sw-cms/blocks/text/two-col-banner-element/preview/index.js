import template from './sw-cms-preview-two-col-banner-element.html.twig';
import './sw-cms-preview-two-col-banner-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-two-col-banner-element', {
    template
});
