import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'two-col-banner-element',
    label: 'sw-cms.blocks.text.twoColBannerElement.label',
    category: 'text',
    component: 'sw-cms-block-two-col-banner-element',
    previewComponent: 'sw-cms-preview-two-col-banner-element',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
	    left: 'two-col-banner-element',
	    right: 'two-col-banner-element'
    }
});
