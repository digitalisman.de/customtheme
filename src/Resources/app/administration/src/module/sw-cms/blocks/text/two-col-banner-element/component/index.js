import template from './sw-cms-block-two-col-banner-element.html.twig';
import './sw-cms-block-two-col-banner-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-block-two-col-banner-element', {
    template
});
