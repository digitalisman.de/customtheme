import template from './sw-cms-preview-three-col-icon-element.html.twig';
import './sw-cms-preview-three-col-icon-element.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-three-col-icon-element', {
    template
});
