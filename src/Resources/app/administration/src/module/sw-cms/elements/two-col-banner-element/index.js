import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'two-col-banner-element',
    label: 'sw-cms.elements.twoColBannerElement.label',
    component: 'sw-cms-el-two-col-banner-element',
    configComponent: 'sw-cms-el-config-two-col-banner-element',
    previewComponent: 'sw-cms-el-preview-two-col-banner-element',
    defaultConfig: {
        horizontalAlign: {
            source: 'static',
            value: false
        },
        verticalAlign: {
            source: 'static',
            value: false
        },
        url: {
            source: 'static',
            value: null
        },
        bannerContent: {
            source: 'static',
            value: null
        },
        buttonText: {
            source: 'static',
            value: 'Button Text'
        },
        backgroundColor: {
            source: 'static',
            value: false
        },
        textColor: {
            source: 'static',
            value: false
        },
        btnColor: {
            source: 'static',
            value: false
        },
        btnOutline: {
            source: 'static',
            value: false
        },
        media: {
            source: 'static',
            value: null,
            required: true,
            entity: {
                name: 'media'
            }
        },
        headline: {
	        source: 'static',
            value: null
        }
    }
});
