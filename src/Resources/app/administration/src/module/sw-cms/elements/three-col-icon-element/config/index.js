import { Component, Mixin } from 'src/core/shopware';
import template from './sw-cms-el-config-three-col-icon-element.html.twig';
//import './sw-cms-el-config-two-col-banner-element.scss';

//const { Component, Mixin } = Shopware;

Component.register('sw-cms-el-config-three-col-icon-element', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('three-col-icon-element');
        }
    }
});
