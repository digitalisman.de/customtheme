import { Component } from 'src/core/shopware';
import template from './sw-cms-el-preview-three-col-icon-element.html.twig';
import './sw-cms-el-preview-three-col-icon-element.scss';

//const { Component } = Shopware;

Component.register('sw-cms-el-preview-three-col-icon-element', {
    template
});
