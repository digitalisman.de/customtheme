import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'three-col-icon-element',
    label: 'sw-cms.elements.threeColIconElement.label',
    component: 'sw-cms-el-three-col-icon-element',
    configComponent: 'sw-cms-el-config-three-col-icon-element',
    previewComponent: 'sw-cms-el-preview-three-col-icon-element',
    defaultConfig: {
        horizontalAlign: {
            source: 'static',
            value: false
        },
        verticalAlign: {
            source: 'static',
            value: false
        },
        url: {
            source: 'static',
            value: null
        },
        bannerContent: {
            source: 'static',
            value: null
        },
        buttonText: {
            source: 'static',
            value: 'Button Text'
        },
        backgroundColor: {
            source: 'static',
            value: false
        },
        textColor: {
            source: 'static',
            value: false
        },
        btnColor: {
            source: 'static',
            value: false
        },
        btnOutline: {
            source: 'static',
            value: false
        },
        media: {
            source: 'static',
            value: null,
            required: true,
            entity: {
                name: 'media'
            }
        },
        headline: {
	        source: 'static',
            value: null
        },
        iconName: {
	        source: 'static',
            value: 'avatar-multiple'
        }
    }
});
