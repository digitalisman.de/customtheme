import { Component } from 'src/core/shopware';
import template from './sw-cms-el-preview-two-col-banner-element.html.twig';
import './sw-cms-el-preview-two-col-banner-element.scss';

//const { Component } = Shopware;

Component.register('sw-cms-el-preview-two-col-banner-element', {
    template
});
