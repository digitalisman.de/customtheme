import { Component, Mixin } from 'src/core/shopware';
import template from './sw-cms-el-two-col-banner-element.html.twig';
import './sw-cms-el-two-col-banner-element.scss';

//const { Component, Mixin } = Shopware;

Component.register('sw-cms-el-two-col-banner-element', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],
    
    computed: {
	    displayModeClass() {
            if (this.element.config.backgroundColor.value === '' || this.element.config.textColor.value === '' || this.element.config.horizontalAlign.value === '' || this.element.config.verticalAlign.value === '') {
                return null;
            }

            return `${this.element.config.backgroundColor.value} ${this.element.config.textColor.value} ${this.element.config.verticalAlign.value} ${this.element.config.horizontalAlign.value}`;
        },
	    mediaUrl() {
            const context = Shopware.Context.api;
            const elemData = this.element.data.media;
            const mediaSource = this.element.config.media.source;

            if (mediaSource === 'mapped') {
                const demoMedia = this.getDemoValue(this.element.config.media.value);

                if (demoMedia && demoMedia.url) {
                    return demoMedia.url;
                }
            }

            if (elemData && elemData.id) {
                return this.element.data.media.url;
            }

            if (elemData && elemData.url) {
                return `${context.assetsPath}${elemData.url}`;
            }

            return `${context.assetsPath}/administration/static/img/cms/preview_mountain_large.jpg`;
        }
    },
    /*
    watch: {
        cmsPageState: {
            deep: true,
            handler() {
                this.$forceUpdate();
            }
        }
    },
	*/
    created() {
        this.createdComponent();
    },
    
    data() {
        return {
            preview1: {}
        }
    },

    methods: {
        createdComponent() {
            this.initElementConfig('two-col-banner-element');
            this.initElementData('two-col-banner-element');
        }
    }
});
